
import machine, time
import urequests as requests
import env

pir_sensor = machine.Pin(4, machine.Pin.IN)
led = machine.Pin(2, machine.Pin.OUT)
led.value(1)


SLEEP_FOR = 10000 # 10 seconds

print("waking up")

def deep_sleep(msecs):
  #configure RTC.ALARM0 to be able to wake the device
  rtc = machine.RTC()
  rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
  # set RTC.ALARM0 to fire after Xmilliseconds, waking the device
  rtc.alarm(rtc.ALARM0, msecs)
  #put the device to sleep
  machine.deepsleep()

def turn_off_light():
    requests.post(env.MOTION_ENDED_ENDPOINT, data='{"motion": "off"}')

def turn_on_light():
    response = requests.post(env.MOTION_DETECTED_ENDPOINT, data='{"motion": "on"}')
    

def callback(p):
    print('pin_change', p, p.value())
    # led.value(abs(1-p.value()))
    if p.value():
        turn_on_light()
    else:
        turn_off_light()
    
def main():
    
    turn_off_light()
    pir_sensor.irq(trigger=machine.Pin.IRQ_RISING | machine.Pin.IRQ_FALLING, handler=callback)

    for i in range(5):
        led.value(0)
        time.sleep(.5)
        led.value(1)
        time.sleep(.5)

main()
