import urequests as requests
import env
import os
import time 
import machine

def update_firmware():
    """Called on boot of the esp8266"""

    urls = (env.GITLAB_URL + 'esp8266/config.json',
             env.GITLAB_URL +'esp8266/pir.py',
             )

    for u in urls:
        print("Downloading ...", u)
        response = requests.get(u)
        fname = u.split('/')[-1]
        with open(fname, 'w') as f:
            print("Writing ...", fname)
            f.write(response.text) 

led = machine.Pin(2, machine.Pin.OUT)
led.value(0)
try: 
    update_firmware()
except:
    # give some visual feedback
    for i in range(3):
        led.value(1);time.sleep(1);led.value(0)
led.value(1)

import pir
    
