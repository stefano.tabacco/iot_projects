# Micropython script to connect to a magichue light

import socket, struct
import os

def _send(sockt, data):
    sockt.send(struct.pack('!%dB' % len(data), *data))

    
ADDR = (os.getenv(MAGICHUE_ADDR), 5577)
sockaddr = socket.getaddrinfo(ADDR[0], ADDR[1])[0][-1]
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connection_result = sock.connect(sockaddr)
on_data = [0x71, 0x23, 0x0f]

_send(sock, on_data)
sock.recv(255)


